# Koina Sales Tool Public API

Version: v1.1.1

Date: June 2022

&copy; 2022 Koina

## Giới thiệu

### Mục đích, phạm vi

Tài liệu mô tả API dành cho các đơn vị liên quan kết nối hệ thống của Koina

### Yêu cầu kỹ thuật

- Kết nối internet _(bắt buộc)_: Dành cho các đơn vị kết nối với hệ thống của Koina thông qua internet
- Khả năng lập trình _(bắt buộc)_: Để kết nối với hệ thống Koina qua API, nhân lực đơn vị kết nối phải có khả năng lập trình và triển khai theo tài liệu API

## Danh sách API

Bên dưới là danh sách các API để kết nối với hệ thống của Koina. Diễn giải chi tiết cho từng API ở từng mục riêng bên dưới.

- `salesToolCreateLead`
- `salesToolDiscountCalculate`
- `salesToolGetPriceTable`
- `salesToolCreateSaleOrder`

### Quy tắc chung

- API cung cấp hỗ trợ các phương thức `GET` và `POST` qua giao thức HTTP
- API có thể gọi từ public và không cần authenticate
- API sử dụng chung một base URL như sau: `https://asia-southeast1-koina-services.cloudfunctions.net`
- Response trả về dưới dạng JSON, với header `Content-Type: application/json; charset=utf-8`
- Phương thức `POST` yêu cầu `body` truyền lên dưới dạng JSON và có header `Content-Type: application/json`
- Khi có lỗi (HTTP status khác `200`), API sẽ trả về response dưới dạng:

```ts
interface ErrorResponse {
  error: string;
}
```

### `salesToolCreateLead`

API này dùng để nhận CRM Lead từ các nguồn khác nhau.

- Endpoint: `/salesToolCreateLead`
- Method: `POST`
- Body:

```ts
interface CreateLeadBody {
  name: string; // Tên Lead
  email: string; // Email
  phone: string; // Số điện thoại
  note: string; // Ghi chú
}
```

- Response:

```ts
interface CreateLeadResponse {
  name: string;
  email: string;
  phone: string;
  note: string;
}
```

- `curl` example:

```bash
curl --request POST \
  --url https://asia-southeast1-koina-services.cloudfunctions.net/salesToolCreateLead \
  --header 'Content-Type: application/json' \
  --data '{
  "name": "Nguyễn Trường",
  "email": "truong@gmail.com",
  "phone": "0987654321",
  "note": "Muốn mua 10 kg cam sành"
}'
```

### `salesToolDiscountCalculate`

API này dùng để tính toán giá trị khuyến mãi và giá đơn vị **cuối cùng** sẽ được áp dụng cho các sản phẩm trong giỏ hàng.

- Endpoint: `https://pricing-old.staging-koinavn.com/api/v1/cart/calculate-price`
- Method: `POST`
- Body:

```ts
type DiscountCalculateBody = {
  meta: {
    category: string; // Danh mục sản phẩm
    sku: string; // SKU
  };
  price: number; // Giá đơn vị
  quantity: number; // Số lượng sản phẩm đã thêm vào giỏ hàng
}[];
```

- Response:

```ts
type DiscountCalculateResponse = {
  meta: {
    category: string;
    sku: string;
  };
  price: number;
  quantity: number;
  discount: number; // Phần trăm chiết khấu
  newPrice: number; // Giá đơn vị cuối cùng của sản phẩm, được sử dụng để tính toán giá trị của giỏ hàng
}[];
```

- `curl` example:

```bash
curl --request POST \
  --url https://pricing-old.staging-koinavn.com/api/v1/cart/calculate-price \
  --header 'Content-Type: application/json' \
  --data '[
{
  "meta": {
    "sku": "10000154",
    "category": "A0121310008"
  },
  "price": 24000,
  "quantity": 50
},
{
  "meta": {
    "sku": "10000155",
    "category": "A0121310008"
  },
  "price": 18000,
  "quantity": 100
}
]'
```

### `salesToolGetPriceTable`

API này dùng để lấy bảng giá đang áp dụng cho từng phân loại đối tượng và khu vực khác nhau.

Đối với query `type`: nếu user đã đăng nhập trên hệ thống của đơn vị, vui lòng để giá trị là `user`, các trường hợp khác để giá trị là `guest`.

Đối với query `location`: hiện tại API đang hỗ trợ khu vực Hồ Chí Minh với giá trị là `hcm`

- Endpoint: `/salesToolGetPriceTable`
- Method: `GET`
- Query:

```ts
interface GetPriceTableQuery {
  type: "user" | "guest"; // Phân loại đối tượng
  location: "hcm"; // Khu vực
}
```

- Response:

```ts
interface GetPriceTableResponse {
  fromDate: string; // Ngày bắt đầu hiệu lực của bảng giá, có format yyyy-MM-dd (Unicode Technical Standard #35)
  toDate: string; // Ngày kết thúc hiệu lực của bảng giá, có format yyyy-MM-dd (Unicode Technical Standard #35)
  id: number;
  items: {
    category: string; // Danh mục sản phẩm
    id: number;
    inventory: number; // Tồn kho đầu ngày
    moq: number; // Số lượng đặt hàng tối thiểu (min order quantity), còn được sử dụng như là bội số khi thêm vào giỏ hàng
    name: string; // Tên sản phẩm
    sku: string; // SKU
    price: number; // Giá đơn vị
    uom: string; // Đơn vị tính (unit of measurement)
  }[];
}
```

- `curl` example:

```bash
curl --request GET \
  --url 'https://asia-southeast1-koina-services.cloudfunctions.net/salesToolGetPriceTable?type=user&location=hcm'
```

### `salesToolCreateSaleOrder`

API này dùng để tạo sale order (đơn hàng bán).

Đối với field `customer`, phần địa chỉ khách hàng chỉ cần truyền `street` (số nhà và tên đường) và `wardCode` (mã phường/xã), hệ thống sẽ tự động điền quận/huyện và tỉnh/thành phố.

Field `customer.wardCode` là mã phường/xã, mã này được lấy từ số liệu của Tổng cục Thống kê [tại đây](https://danhmuchanhchinh.gso.gov.vn/).

- Endpoint: `/salesToolCreateSaleOrder`
- Method: `POST`
- Body:

```ts
type CreateSaleOrderBody = {
  customer: {
    name: string; // Tên khách hàng
    phone: string; // Số điện thoại khách hàng
    street: string; // Số nhà và tên đường (vd: 39 Đoàn Như Hài)
    wardCode: string; // Mã phường/xã theo dữ liệu của Tổng cục Thống Kê (vd: 27259 là Phường 13, Quận 4, Thành Phố Hồ Chí Minh).
  };
  deliveryDate: string; // Ngày giao hàng, format dưới dạng ISO 8601
  deliveryTimeFrame: "03:00 - 07:00" | "09:00 - 12:00" | "13:00 - 16:00"; // Khung giờ giao hàng
  orderLines: {
    discount: number; // Phần trăm khuyến mãi
    price: number; // Giá đơn vị
    quantity: number; // Số lượng sản phẩm
    sku: string; // SKU
    uom: string; // Đơn vị tính (unit of measurement)
  }[];
  paymentMethod: "cod" | "paid"; // Hình thức thanh toán
  pricelistId: number; // ID của bàng giá
};
```

- Response:

```ts
type CreateSaleOrderResponse = {
  soId: string; // Mã đơn hàng được tạo trên hệ thống
};
```

- `curl` example:

```bash
curl --request POST \
  --url https://asia-southeast1-koina-services.cloudfunctions.net/salesToolCreateSaleOrder \
  --header 'Content-Type: application/json' \
  --data '{
  "customer": {
    "name": "Nguyễn Trường",
    "phone": "0987644321",
    "street": "69/96 Đường Đi",
    "wardCode": "26959"
  },
  "deliveryDate": "2022-04-25T03:06:27.676Z",
  "deliveryTimeFrame": "03:00 - 07:00",
  "orderLines": [
    {
      "discount": 3,
      "price": 20000,
      "quantity": 50,
      "sku": "10000154",
      "uom": "kg"
    }
  ],
  "paymentMethod": "cod",
  "pricelistId": 466
}'
```
